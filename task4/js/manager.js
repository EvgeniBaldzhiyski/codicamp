/**
 * Desktop managee
 * 
 * @author Evgeni Baldzhiyski
 * @version 0.1
 *
 * @events {
 *		win-minimize
 *		win-maximize
 *		win-close
 *		win-move
 *		win-resize
 *		win-open
 *		win-focus
 * }
 * @param settings {Object{
 *	 	winTemplate
 *	 	winButtonTemplate
 *	 	wall
 *	 	buttonBar
 * }}
 * return {Object{
 *		function openWindow (url, title, img, x, y, w, h)
 * 		function moveWindow (id, x, y)
 *		function resizeWindow (id, w, h)
 *		function closeWindow (id)
 *		function minimizeWindow (id)
 *		function maximizeWindow (id)
 *		function showWindow (id)
 * }}
 */
window.DesktopManager = function (settings){
	var self = document.createElement('div');
	
	var _windex 	= 0,
		_windows 	= [],
		_tplWinItem = document.getElementById(settings.winTemplate),
		_tplWinBtn 	= document.getElementById(settings.winButtonTemplate),
	
		wall 		= document.getElementById(settings.wall),
		bar 		= document.getElementById(settings.buttonBar);	
	
	/**
	 * @param id {String}
	 * @param title {String}
	 * @param img {String}
	 * @return {DOMNode extends{
	 *		function zindex (z)
	 *		function select (value)
	 *		function body (html)
	 *		function destruct ()
	 * }}
	 */
	function UIWindow(id, title, img){
		var tpl = document.createElement('div');
		
		tpl.innerHTML = _tplWinItem.outerHTML;
		var obj = tpl.childNodes[0];
		
		tpl.innerHTML = _tplWinBtn.outerHTML;
		var btn = tpl.childNodes[0];
			btn.id = 'b'+id;
		
		var _className = obj.className,
			_btnClassName = btn.className,
		
			_selected = false,
		
			_clickX = 0,
			_clickY = 0,
		
			titleObj 	= obj.querySelector(".window-title"),
			minBtn 	 	= obj.querySelector(".window-button-min"),
			maxBtn 	 	= obj.querySelector(".window-button-max"),
			closeBtn 	= obj.querySelector(".window-button-close"),
			titleText 	= obj.querySelector(".window-title-text"),
			body 	 	= obj.querySelector(".window-body"),
			resizeBox 	= obj.querySelector(".resize-box");
		
		/**
		 * Buttons (close, minimize, maximize) elent handler
		 *
		 * @param {Event}
		 */
		function onActions(e){
			var action = '';
			
			switch(e.target.className){
				case 'window-button-min':
					action = 'win-minimize'; break;
				case 'window-button-max':
					action = 'win-maximize'; break;
				case 'window-button-close':
					action = 'win-close'; break;
			}
			e.stopPropagation();
			
			if(action){
				EventsLib.trigger(EventsLib.create(action, id), self);
			}
		}
		/**
		 * Window focus handler. Run when have click samewhere in the window. 
		 *
		 * @param {Event}		 
		 */
		function onFocus(e){
			EventsLib.trigger(EventsLib.create('win-focus', id), self);
		}
		/**
		 * Window button click handler. Run when have click samewhere in the button in the button bar. 
		 *
		 * @param {Event}		 
		 */		
		function onButtonClick(e){
			EventsLib.trigger(EventsLib.create('win-open', id), self);
		}
		/**
		 * Start move event handler. Run when have mouse down samewhere in the title bar. 
		 *
		 * @param {Event}		 
		 */		
		function onTitleBarDown(e){
			_clickX = e.pageX;
			_clickY = e.pageY;
			
			EventsLib.bind('mouseup', onTitleBarUp, document);
			EventsLib.bind('mousemove', onWinMove, document);
		}
		/**
		 * Stop move event handler. Run when have mouse up samewhere in the document.
		 *
		 * @param {Event}		 
		 */		
		function onTitleBarUp(e){
			EventsLib.unbind('mouseup', onTitleBarUp, document);
			EventsLib.unbind('mousemove', onWinMove, document);
		}
		/**
		 * Progress move event handler. Run when mouse move in the document. 
		 *
		 * @param {Event}		 
		 */		
		function onWinMove(e){
			_onGlobalMouseMove('win-move', e.pageX, e.pageY);
		}		
		/**
		 * Start resize event handler. 
		 *
		 * @param {Event}		 
		 */		
		function onResizeBoxDown(e){
			_clickX = e.pageX;
			_clickY = e.pageY;
			
			EventsLib.bind('mouseup', onResizeBoxUp, document);
			EventsLib.bind('mousemove', onResizeMove, document);
		}
		/**
		 * Stop resize event handler. Run when have mouse up samewhere in the document.
		 *
		 * @param {Event}		 
		 */		
		function onResizeBoxUp(e){
			EventsLib.unbind('mouseup', onResizeBoxUp, document);
			EventsLib.unbind('mousemove', onResizeMove, document);
		}
		/**
		 * Progress resize event handler. Run when mouse move in the document. 
		 *
		 * @param {Event}		 
		 */		
		function onResizeMove(e){
			_onGlobalMouseMove('win-resize', e.pageX, e.pageY);
		}		
		
		/**
		 * Mouse move event dispacher. 
		 *
		 * @param {name}
		 * @param {Number}
		 * @param {Number}
		 */	
		function _onGlobalMouseMove(name, x, y){
			EventsLib.trigger(EventsLib.create(name, {
				id: id,
				ox: _clickX,
				oy: _clickY,
				nx: (_clickX = x),
				ny: (_clickY = y)
			}), self);
		}
		
		obj.id = id;
		/**
		 * System public register.
		 *
		 * @var {Object}
		 */
		obj.ui = {
			img		: img,
			title	: title,
			button	: btn
		};
		/**
		 * 3D ordering.
		 *
		 * @param [z] {Integer}
		 * @return {Integer}
		 */
		obj.zindex = function(z){
			if(typeof z != 'undefined'){
				obj.style.zIndex = z;
			}
			return obj.style.zIndex;
		};
		/**
		 * Class changer.
		 *
		 * @param [value] {Boolean}
		 * @return {Boolean}
		 */		
		obj.select = function (value){
			if(typeof value != 'undefined'){
				_selected = value;
				
				if(value){
					obj.className = _className+' selected';
					btn.className = _btnClassName+' selected';
				}else{
					obj.className = _className;
					btn.className = _btnClassName;
				}
			}
			return _selected;
		};
		/**
		 * Content changer.
		 *
		 * @param html {String}
		 * @return {void}
		 */			
		obj.body = function (html){
			body.innerHTML = html;
		};
		/**
		 * Event and register cleanner.
		 *
		 * @return {void}
		 */			
		obj.destruct = function (){
			EventsLib.unbind('click', onActions, minBtn);
			EventsLib.unbind('click', onActions, maxBtn);
			EventsLib.unbind('click', onActions, closeBtn);
			
			EventsLib.unbind('mousedown', onFocus, obj);
			
			EventsLib.unbind('click', onButtonClick, btn);
			
			obj.ui = null;
		};

		EventsLib.bind('click', onActions, minBtn);
		EventsLib.bind('click', onActions, maxBtn);
		EventsLib.bind('click', onActions, closeBtn);
		
		EventsLib.bind('mousedown', onFocus, obj);
		
		EventsLib.bind('click', onButtonClick, btn);
		
		EventsLib.bind('mousedown', onTitleBarDown, titleObj);
		EventsLib.bind('mousedown', onResizeBoxDown, resizeBox);
		
		titleText.innerText = title;
		titleObj.style.backgroundImage = "url("+img+")";
		
		btn.innerText = title;
		btn.style.backgroundImage = "url("+img+")";
		
		btn.style.display = 'block';
		
		return obj;
	}
	
	// Window manager
	
	/**
	 * Create new window.
	 *
	 * @param url {String}
	 * @param title {String}
	 * @param img {String}
	 * @return {String}
	 */
	function create(url, title, img){
		var _lx = 10,
			_ly = 10,
			_lw = 300,
			_lh = 400,
			_w  = _windows[_windows.length - 1];
			
		if(_w){
			_lx += _w.ui.lx;
			_ly += _w.ui.ly;
			_lw  = _w.ui.lw;
			_lh  = _w.ui.lh;
		}
		
		var win = UIWindow(_windex, title, img);
			win.style.left 	 = (win.ui.lx = _lx)+'px';
			win.style.top 	 = (win.ui.ly = _ly)+'px';
			win.style.width  = (win.ui.lw = _lw)+'px';
			win.style.height = (win.ui.lh = _lh)+'px';
		
			win.ui.url = url;
		
		ContentLoader.load(url, win.body);
		
		wall.appendChild(win);
		bar.appendChild(win.ui.button);
		
		_windows.push(win);
		
		_windex++;
		
		_onOpen(win.id);
		
		return win.id;
	}
	/**
	 * Maximize/return back in normal state the window.
	 *
	 * @param e {Event}
	 * @return {void}
	 */
	function onMaximize(e){
		var el = document.getElementById(e.data);
			
		if(!el._logState){	
			el._logState = {
				x: el.style.left,
				y: el.style.top,
				w: el.style.width,
				h: el.style.height
			}
			
			var wall = el.parentNode;
			
			el.style.left 	= '0px';
			el.style.top 	= '0px';
			el.style.width 	= el.parentNode.clientWidth+'px';
			el.style.height = el.parentNode.clientHeight+'px';
		}else{
			el.style.left 	= el._logState.x;
			el.style.top 	= el._logState.y;
			el.style.width 	= el._logState.w;
			el.style.height = el._logState.h;
			
			el._logState = null;
		}
		_onFocus(el.id);
	}
	/**
	 * Minimize the window.
	 *
	 * @param e {Event}
	 * @return {void}
	 */	
	function onMinimize(e){
		_onMinimize(e.data);
	}
	/**
	 * Minimize/return to normal state the window.
	 *
	 * @param e {Event}
	 * @return {void}
	 */		
	function onOpen(e){
		_onOpen(e.data);
	}
	/**
	 * Move the window.
	 *
	 * @param e {Event}
	 * @return {void}
	 */		
	function onMove(e){
		var win = document.getElementById(e.data.id);
		
		win.ui.lx += (e.data.nx - e.data.ox);
		win.ui.ly += (e.data.ny - e.data.oy);
		
		win.style.left 	 = (win.ui.lx)+'px';
		win.style.top 	 = (win.ui.ly)+'px';		
	}	
	/**
	 * Move the window.
	 *
	 * @param e {Event}
	 * @return {void}
	 */		
	function onResize(e){
		var win = document.getElementById(e.data.id);
		
		win.ui.lw += (e.data.nx - e.data.ox);
		win.ui.lh += (e.data.ny - e.data.oy);
		
		win.style.width  = (win.ui.lw)+'px';
		win.style.height = (win.ui.lh)+'px';		
	}
	/**
	 * Focus the window.
	 *
	 * @param e {Event}
	 * @return {void}
	 */		
	function onFocus(e){
		_onFocus(e.data);
	}
	/**
	 * Remove the window.
	 *
	 * @param e {Event}
	 * @return {void}
	 */		
	function onRemove(e){
		var id  = e.data,
			sel = false,
			win = document.getElementById(id);
			
		wall.removeChild(win);	
		bar.removeChild(win.ui.button);
		
		win.destruct();
		
		var i; for(i = 0; i < _windows.length; i++){
			if(_windows[i].id == id){
				_windows.splice(i, 1);
				
				continue;
			}
			
			if(_windows[i].select()){
				sel = true;
			}
		}
		
		if(!sel && _windows.length){
			for(i = _windows.length - 1; i >= 0; i--){
				if(_windows[i].style.display != 'none'){
					_onFocus(_windows[i].id);
					
					break;
				}
			}
		}
	}

	/**
	 * Page list item click event.
	 *
	 * @param e {Event}
	 * @return {void}
	 */
	function _onFocus(id){
		var target;
		_windows.forEach(function (el, i){
			if(el.id == id){
				target = el;
				
				var next = _windows[i + 1]; if(next){
					_windows[i + 1] = el;
					_windows[i] = next;
				}
			}
			_windows[i].select(false);
			_windows[i].zindex(i);
		});
		if(target){
			target.select(true);
		}
	}
	
	// System methods
	
	/**
	 * Minimize/focus the window.
	 *
	 * @param id {String}
	 * @return {void}
	 */	
	function _onOpen(id){
		var el = document.getElementById(id);
		
		if(el.select()){
			_onMinimize(el.id);
		}else{
			el.style.display = 'block';
			
			_onFocus(el.id);
		}
	}
	/**
	 * Minimize the window.
	 *
	 * @param id {String}
	 * @return {void}
	 */	
	function _onMinimize(id){
		var el = document.getElementById(id);
			el.style.display = 'none';
			el.select(false);
		
		var i; for(i = _windows.length - 1; i >= 0; i--){
			if(_windows[i].id != id && _windows[i].style.display != 'none'){
				_onFocus(_windows[i].id); break;
			}
		}
	}
	
	/**
	 * Public API
	 */
	 
	/**
	 * Create ans show window.
	 *
	 * @param url {String}
	 * @param title {String}
	 * @param img {String}
	 * @param [x] {String}
	 * @param [y] {String}
	 * @param [w] {String}
	 * @param [h] {String}
	 * @return {String}
	 */	 
	self.openWindow = function (url, title, img, x, y, w, h){
			var id = create(url, title, img);
			
			if(x || y || w || h){
				var win = document.getElementById(id);
				
				if(x) win.style.left = x+'px';
				if(y) win.style.top = y+'px';
				if(w) win.style.width = w+'px';
				if(h) win.style.height = h+'px';
			}
			return id;
	};
	/**
	 * Move window to.
	 *
	 * @param id {String}
	 * @param x {String}
	 * @param y {String}
	 * @return {void}
	 */			
	self.moveWindow = function (id, x, y){
			
	};
	/**
	 * Resize window to.
	 *
	 * @param id {String}
	 * @param w {String}
	 * @param h {String}
	 * @return {void}
	 */		
	self.resizeWindow = function (id, w, h){
			
	};
	/**
	 * Close window to.
	 *
	 * @param id {String}}
	 * @return {void}
	 */		
	self.closeWindow = function (id){
			
	};
	/**
	 * Minimize window to.
	 *
	 * @param id {String}}
	 * @return {void}
	 */		
	self.minimizeWindow = function (id){
			
	};
	/**
	 * Maximize window to.
	 *
	 * @param id {String}}
	 * @return {void}
	 */		
	self.maximizeWindow = function (id){
			
	};
	/**
	 * If window is minimized return show state. Minimize window if it is in normal state.
	 *
	 * @param id {String}}
	 * @return {void}
	 */		
	self.showWindow = function (id){
			
	};
	
	EventsLib.bind('win-minimize', 	onMinimize, self);
	EventsLib.bind('win-maximize', 	onMaximize, self);
	EventsLib.bind('win-close', 	onRemove, self);
	EventsLib.bind('win-move', 		onMove, self);
	EventsLib.bind('win-resize', 	onResize, self);
	EventsLib.bind('win-open', 		onOpen, self);
	EventsLib.bind('win-focus', 	onFocus, self);
	
	return self;
};