/**
 * Manager of the events.
 *
 * @author Evgeni Baldzhiyski
 * @version 0.1
 *
 * @return {Object{
 *		create:  function (name, data)
 *		bind: 	 function (name, callback, obj)
 *		unbind:  function (name, callback, obj)
 *		trigger: function (ev, obj)
 * }}
 */
window.EventsLib = (function (){
	var worker = document.createElement('div');
	
	return {
		/**
		 * @param name {String}
		 * @param data {Object}
		 * @return {void}				 
		 */
		create: function(name, data){
			var ev;
			try{
				ev = new CustomEvent(name);
			}catch(error){
				ev = document.createEvent("CustomEvent");	
				ev.initCustomEvent(name, true, true, "window", 0, 0, 0, 0, 0, false, false, false, false, 0, null);
			}
			ev.data = data;
			
			return ev;
		},
		/**
		 * @param name {String}
		 * @param callback {Function}
		 * @param [obj] {DOMNode}
		 * @return {void}				 
		 */			
		bind: function (name, callback, obj){
			obj = obj || worker;
			
			obj.addEventListener(name, callback);
		},
		/**
		 * @param name {String}
		 * @param callback {Function}
		 * @param [obj] {DOMNode}
		 * @return {void}				 
		 */				
		unbind: function (name, callback, obj){
			obj = obj || worker;
			
			obj.removeEventListener(name, callback);
		},
		/**
		 * @param ev {Event}
		 * @param [obj] {DOMNode}
		 * @return {void}			 
		 */				
		trigger: function (ev, obj){
			obj = obj || worker;
			
			obj.dispatchEvent(ev);
		}
	}
})();