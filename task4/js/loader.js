/**
 * Manager of the requests.
 *
 * @author Evgeni Baldzhiyski
 * @version 0.1
 *
 * @return {Object{
 *		load: function (url, target)	 
 * }}
 */
window.ContentLoader = (function (){
	/**
	 * Register of requests
	 *
	 * @var {Array [{url, data}] } 
	 */
	var cache = [];
	/**
	 *	Request responce callback.
	 *
	 *	@return {void}
	 */
	function reqListener () {
		console.log(this.responseText);
		
		cache.push({
			url: this.__url,
			data: this.responseText
		});
		
		this.__target(this.responseText);
	}
	
	return {
		/**
		 * Load data 
		 *
		 * @param url {String}
		 * @param target {Function}
		 * @return {void}				 
		 */
		load: function (url, target){
			var data = '';
		
			var i; for(i = 0; i < cache.length; i++){
				if(cache[i].url == url){
					data = cache[i].data;
					
					break;
				}
			}
			
			if(data){
				target(data);
			}else{
				var oReq = new XMLHttpRequest();
					oReq.addEventListener("load", reqListener);
					oReq.__target = target;
					oReq.__url = url;
					oReq.open("GET", url+'?'+(new Date).getTime());
					oReq.send();
			}
		}
	}
})();