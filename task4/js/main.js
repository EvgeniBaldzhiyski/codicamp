/**
 * Virtual Desktop
 *
 * @author Evgeni Baldzhiyski
 * @version 0.1
 */
(function (){
	var list  = document.getElementById("pageList"),
		pages = list.querySelectorAll('li'),
		start = document.getElementById('startButton');
	
	var manager = DesktopManager({
		winTemplate			: 'winItemProto',
		winButtonTemplate	: 'winBtnProto',
		wall				: 'wall',
		buttonBar			: 'buttonBar'
	});	
	/**
	 * Close the page list box. Run when have click samewhere in the document.
	 *
	 * @param e {Event}
	 * @return {void}
	 */	
	function onStartClose(e){
		EventsLib.unbind('click', onStartClose, document);
		
		list.style.display = 'none';
	}
	/**
	 * Open the page list box. Run when have click on the start button.
	 *
	 * @param e {Event}
	 * @return {void}
	 */
	function onStartOpen(e){
		list.style.display = 'block';
		
		e.stopPropagation();
		
		EventsLib.bind('click', onStartClose, document);
	}
	/**
	 * Open new window.
	 *
	 * @param e {Event}
	 * @return {void}
	 */	
	function onOpenPage(e){
		manager.openWindow(
			e.target.dataset.url, 
			e.target.dataset.title, 
			e.target.dataset.img
		);
	}	
	
	EventsLib.bind('click', onStartOpen, start);	

	if(pages){
		var p; for(p in pages){
			EventsLib.bind('click', onOpenPage, pages[p]);
		};
	}
})();
